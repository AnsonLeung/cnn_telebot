

var cmd = require('node-cmd');
const TeleBot = require('telebot');
const bot = new TeleBot('255849038:AAEiOSA4k6KYrHXF7d3XMk658KdtxwRNYBM');

bot.on('/start', msg => {
	let fromId = msg.from.id;
	let firstName = msg.from.first_name;
	let reply = msg.message_id;
	return bot.sendMessage(fromId, `Welcome, ${ firstName }!`, { reply });
});

bot.on('/log', msg => {
	let fromId = msg.from.id;
	let reply = msg.message_id;
	cmd.get(
		'cat ../current_experiment',
		function(data){
			data = data.replace(/(\r\n|\n|\r)/gm,"");
			command = 'cat ../brain_segmentation_cnn/experiments/'+data+'/log.log';
			console.log(command); 
			cmd.get(
				command,
				function(data2){
					response = data2.split("Epoch");
					response = "Epoch "+response[response.length-2] + "Epoch "+response[response.length-1];
					//console.log(response);
					return bot.sendMessage(fromId, response, { reply });
				});
		});
});

bot.on('/log5', msg => {
	let fromId = msg.from.id;
	let reply = msg.message_id;
	cmd.get(
		'cat ../current_experiment',
		function(data){
			data = data.replace(/(\r\n|\n|\r)/gm,"");
			command = 'cat ../brain_segmentation_cnn/experiments/'+data+'/log.log';
			// console.log(command); 
			cmd.get(
				command,
				function(data2){
					data2 = data2.split("Epoch");
					response = ""
					for(i=1; i<=5; i++)
						response = "Epoch "+data2[data2.length-i] + response;
					//console.log(response);
					return bot.sendMessage(fromId, response, { reply });
				});
		});
});

bot.on('/en_now', msg => {
	let fromId = msg.from.id;
	let reply = msg.message_id;
	command = 'cat ../brain_segmentation_cnn/experiments/ensemble/log.log';
	// console.log(command); 
	cmd.get(
		command,
		function(data){
			data = data.split("============================================================");
			response = data[data.length-1];
	//console.log(response);ƒ
	return bot.sendMessage(fromId, response, { reply });
});
});

bot.on('/en_results', msg => {
	let fromId = msg.from.id;
	let reply = msg.message_id;
	command = 'cat ../brain_segmentation_cnn/experiments/ensemble/log.log';
	// console.log(command); 
	cmd.get(
		command,
		function(data){
			results = [];
			re_start = "It took";
			re_end = "The overall error rate";
			start = -1;
			end = -1
			while(true){
				start = results.indexOf(re_start, end+1);
				if (start < 0)
					break;
				end = results.indexOf(re_end, start+1);
				end += 5;
				length = start - end + 1;
				results.append(data.substring(start,length));
			}

			reply = "No. of MRI evaluated: " + results.length;
			for (int i=0; i<results.length; i++){
				reply += "\n=====================\n";
				reply += "MRI " + i + ":\n";
				reply += results[i] + "\n";
			}

	//console.log(response);ƒ
	return bot.sendMessage(fromId, response, { reply });
});
});

bot.connect();
